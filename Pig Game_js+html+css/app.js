/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var pigGame = (function () {

    var _data = {
        scores: [],
        roundScore: 0,
        activePlayer: 0,
        isGamePlaying: false
    };

    var _domElements = {

        firstDice: document.getElementById("dice-1"),
        secondDice: document.getElementById("dice-2"),
        currentScorePlayer1: document.getElementById("current-0"),
        currentScorePlayer2: document.getElementById("current-1"),
        panelPlayer1: document.querySelector(".player-0-panel"),
        panelPlayer2: document.querySelector(".player-1-panel"),
        firstScore: document.getElementById("score-0"),
        secondScore: document.getElementById("score-1"),
        player1: document.getElementById("name-0"),
        player2: document.getElementById("name-0")
    };

    
    document.querySelector(".btn-roll").addEventListener("click", function() {
        if(_data.isGamePlaying) {
            var dice1 = Math.floor(Math.random() * 6) + 1;
            var dice2 = Math.floor(Math.random() * 6) + 1;
            
            _domElements.firstDice.style.display = "block";
            _domElements.secondDice.style.display = "block";
            _domElements.firstDice.src = "dice-" + dice1 + ".png";
            _domElements.secondDice.src = "dice-" + dice2 + ".png";
            
            if(dice1 !== 1 && dice2 !== 1) {
                _data.roundScore += dice1 + dice2;
                document.querySelector("#current-" + _data.activePlayer).textContent = _data.roundScore;
            } else {
                nextPlayer(); 
            }
         }
    });
    
    document.querySelector(".btn-hold").addEventListener("click", function() {
        if(_data.isGamePlaying) {
            _data.scores[_data.activePlayer] += _data.roundScore;
            
            document.getElementById("score-" + _data.activePlayer).textContent = _data.scores[_data.activePlayer];
            
            var input = document.querySelector(".final-score").value;
            var winningScore;
            if (input && parseInt(input)) {
                winningScore = input;
            } else {
                winningScore = 100;
            }
            
            if (_data.scores[_data.activePlayer] >= winningScore) {
                document.getElementById("name-" + _data.activePlayer).textContent = "Winner!!!";
                hideDices();
                document.querySelector(".player-" + _data.activePlayer + "-panel").classList.toggle("winner");
                document.querySelector(".player-" + _data.activePlayer + "-panel").classList.remove("active");
                _data.isGamePlaying = false;
            } else {
                nextPlayer();
            }
        }
    });
    
    document.querySelector(".btn-new").addEventListener("click", _init);
    
    function nextPlayer() {
        _data.activePlayer = _data.activePlayer === 1 ? 0 : 1;
        _data.roundScore = 0;
        _domElements.currentScorePlayer1.textContent = "0";
        _domElements.currentScorePlayer2.textContent = "0";   
        _domElements.panelPlayer1.classList.toggle("active");
        _domElements.panelPlayer2.classList.toggle("active");
        hideDices();    
    };
    
    function hideDices() {
        _domElements.firstDice.style.display = "none";
        _domElements.secondDice.style.display = "none";
    };

    function _init() {
        _data.scores = [0, 0];
        _data.activePlayer = 0;
        _data.roundScore = 0;
        _data.isGamePlaying = true;
        hideDices();
        _domElements.firstScore.textContent = "0";
        _domElements.secondScore.textContent = "0";
        _domElements.currentScorePlayer1.textContent = "0";
        _domElements.currentScorePlayer2.textContent = "0";
        _domElements.player1.textContent = "Player 1";
        _domElements.player2.textContent = "Player 2";
        _domElements.panelPlayer1.classList.remove("winner");
        _domElements.panelPlayer2.classList.remove("winner");
        _domElements.panelPlayer1.classList.remove("active");
        _domElements.panelPlayer2.classList.remove("active");
        _domElements.panelPlayer1.classList.add("active");
    }

     document.addEventListener("DOMContentLoaded", _init);

})();
