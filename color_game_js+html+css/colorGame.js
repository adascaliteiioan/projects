//Add comments inline and short

(function (cg) {

  var _data = {
    numSquares: 6, //name = squares
    colors: [],
    pickedColor: "" //name = just color
  };

  //rename with underscore _dom
  var _DOM = {
    squares: document.querySelectorAll(".square"),//squares: $(".square") 
    modeButtons: document.querySelectorAll(".mode"),
    colorDisplay: document.getElementById("colorDisplay"),
    displayedColor: document.querySelector("h1"),//change into name for function
    messageDisplay: document.querySelector("#message"),
    resetButton: document.querySelector("#reset")
  };

  function _init() {
    console.log("init called");
      _setupModeButtons();
      _setupSquares();
      _reset();
    };
//private functions

  function _setupModeButtons() {
    
    _DOM.resetButton.addEventListener("click", _reset);
  	for (var i = 0; i < _DOM.modeButtons.length; i++) {
  		  _DOM.modeButtons[i].addEventListener("click", function() {
  			     _DOM.modeButtons[0].classList.remove("selected");
  			     _DOM.modeButtons[1].classList.remove("selected");
  			     this.classList.add("selected");
  			     this.textContent === "Easy" ? _data.numSquares = 3 : _data.numSquares = 6;
  			     _reset();
  		});
  	}
  }

  function _setupSquares() {
  	for (var i = 0; i < _DOM.squares.length; i++) {
  		_DOM.squares[i].addEventListener("click", function() {
  			var color = this.style.background;
  			if (color == _data.pickedColor) {
  				_DOM.messageDisplay.textContent = "Correct!";
  				_DOM.resetButton.textContent = "Play Again?";
          _DOM.squares.forEach(function(element) {
              element.style.background = color;
              console.log("element = " + element+ " color = "+ color);
          });
  				_DOM.displayedColor.style.background = color;
  			} else {
  				this.style.background = "#232323";
  				_DOM.messageDisplay.textContent = "Try Again";
  			}
  		});
  	}
  }

  function _reset() {
  	_data.colors = _generateRandomColors(_data.numSquares);
  	_data.pickedColor = _data.colors[Math.floor(Math.random() * _data.colors.length)];
  	_DOM.colorDisplay.textContent = _data.pickedColor;
    _DOM.resetButton.textContent = "New Colors";
  	_DOM.messageDisplay.textContent = "";
  	for (var i = 0; i < _DOM.squares.length; i++) {
  		if (_data.colors[i]) {
  			_DOM.squares[i].style.display = "block";
  			_DOM.squares[i].style.background = _data.colors[i];
  		} else {
  			_DOM.squares[i].style.display = "none";
  		}
  	}
  	_DOM.h1.style.background = "steelblue";
  }

  function _generateRandomColors(num) {
  	var arr = [];
  	for (var i = 0; i < num; i++) {
  		arr.push(_randomColor());
  	}
  	return arr;
  }

  function _randomColor() {
  	var r = Math.floor(Math.random() * 256);
  	var g = Math.floor(Math.random() * 256);
  	var b = Math.floor(Math.random() * 256);
  	return "rgb(" + r + ", " + g + ", " + b + ")";
  }


  document.addEventListener("DOMContentLoaded", _init); 

})(cg = {});
